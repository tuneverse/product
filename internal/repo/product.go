package repo

import (
	"context"
	"database/sql"
	"product/internal/consts"
	"product/internal/entities"

	"gitlab.com/tuneverse/toolkit/core/logger"
)

// ProductRepo represents the repository for the Product entity.
type ProductRepo struct {
	db *sql.DB
}

// ProductRepoImply is an interface specifying the methods for the Product repository.
type ProductRepoImply interface {
	IsProductExists(context.Context, string) (bool, error)
	CreateComments(context.Context, string, string, entities.ReviewComments) error
}

// NewProductRepo creates a new instance of ProductRepo.
func NewProductRepo(db *sql.DB) ProductRepoImply {
	return &ProductRepo{
		db: db,
	}
}

func (repo *ProductRepo) IsProductExists(ctx context.Context, productID string) (bool, error) {

	var exists int
	isProductExistsQ := `SELECT 1 FROM product WHERE id = $1 AND  is_deleted =$2 ANd is_active =$3`
	row := repo.db.QueryRowContext(ctx, isProductExistsQ, productID, false, true)
	err := row.Scan(&exists)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
func (repo *ProductRepo) CreateComments(ctx context.Context, productID string, MemberID string, comments entities.ReviewComments) error {
	query := `INSERT INTO product_review_comment (product_id,member_id,comment)VALUES($1,$2,$3)`
	_, err := repo.db.ExecContext(ctx, query, productID, MemberID, comments.Comments)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf(consts.CreateCommentsErrMsg, err.Error())
		return err
	}
	return nil
}
