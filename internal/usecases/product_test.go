package usecases

import (
	"context"
	"product/internal/consts"
	"product/internal/entities"
	"product/internal/repo/mock"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
)

var (
	logLevel = "info"
)

func TestCreateComments(t *testing.T) {

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            logLevel,
		IncludeRequestDump:  false,
		IncludeResponseDump: true,
	}
	_ = logger.InitLogger(clientOpt)

	testCases := []struct {
		name          string
		productId     string
		MemberId      string
		comments      entities.ReviewComments
		buildStubs    func(product *mock.MockProductRepoImply)
		checkResponse func(t *testing.T, err error, erMap map[string]models.ErrorResponse)
	}{
		{
			name:      "ok",
			productId: "a2a00db1-3c9a-4b75-9907-6ef55b63d379",
			MemberId:  "980e783c-e664-452d-b1ff-30d2e7767024",
			comments: entities.ReviewComments{
				Comments: "comment",
			},
			buildStubs: func(product *mock.MockProductRepoImply) {
				product.EXPECT().
					CreateComments(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
					Times(1)

			},
			checkResponse: func(t *testing.T, err error, erMap map[string]models.ErrorResponse) {
				require.Nil(t, err)
			},
		},
		{
			name:      "validation error for required fields",
			productId: "a2a00db1-3c9a-4b75-9907-6ef55b63d379",
			MemberId:  "980e783c-e664-452d-b1ff-30d2e7767024",
			comments: entities.ReviewComments{
				Comments: "",
			},
			buildStubs: func(product *mock.MockProductRepoImply) {
				product.EXPECT().
					CreateComments(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
					Times(0)

			},
			checkResponse: func(t *testing.T, err error, erMap map[string]models.ErrorResponse) {
				require.NotEqual(t, 0, len(erMap))
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			product := mock.NewMockProductRepoImply(ctrl)
			tc.buildStubs(product)
			productUsecase := NewProductUseCases(product, nil)

			errmap, err := productUsecase.CreateComments(context.Background(), tc.productId, tc.MemberId, tc.comments, "", "", map[string]models.ErrorResponse{})
			tc.checkResponse(t, err, errmap)

		})
	}
}
