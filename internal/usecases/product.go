package usecases

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	cloud "product/internal/cloud/awsutils"
	"product/internal/consts"
	"product/internal/entities"
	"product/internal/repo"
	"product/utilities"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// ProductUseCases struct represents the use cases for the Product entity.
type ProductUseCases struct {
	repo  repo.ProductRepoImply
	cloud cloud.CloudServiceImply
}

// ProductUseCaseImply is an interface specifying the methods for Product use cases.
type ProductUseCaseImply interface {
	IsProductExists(context.Context, string, string, string, map[string]models.ErrorResponse) (bool, error)
	IsMemberExists(context.Context, string, string, string, map[string]models.ErrorResponse) error
	ExtractMemberDetails(context.Context, string) (string, error)
	CreateComments(context.Context, string, string, entities.ReviewComments, string, string, map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error)
}

// NewProductUseCases creates a new instance of ProductUseCases.
func NewProductUseCases(productRepo repo.ProductRepoImply, cloud cloud.CloudServiceImply) ProductUseCaseImply {
	return &ProductUseCases{
		repo:  productRepo,
		cloud: cloud,
	}
}

func (product *ProductUseCases) IsProductExists(ctx context.Context, ProductID string, endpoint string, method string, errMap map[string]models.ErrorResponse) (bool, error) {

	_, err := uuid.Parse(ProductID)

	if err != nil {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.ProductIDKey, consts.InvalidKey)
		if err != nil {
			logger.Log().WithContext(ctx).Error("IsProductExists failed", err)
		}

		errMap[consts.ProductIDKey] = models.ErrorResponse{
			Code:    code,
			Message: []string{consts.InvalidKey},
		}
		return false, err
	}
	exists, err := product.repo.IsProductExists(ctx, ProductID)
	if exists {
		return true, nil
	}

	if err != nil {
		return false, err
	}
	code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.ProductIDKey, consts.NotFoundKey)
	if err != nil {
		logger.Log().WithContext(ctx).Error("IsProductExists failed", err)
	}
	errMap[consts.ProductIDKey] = models.ErrorResponse{
		Code:    code,
		Message: []string{consts.NotFoundKey},
	}
	return false, nil
}

func (product *ProductUseCases) IsMemberExists(ctx context.Context, memberID string, endpoint string, method string, errMap map[string]models.ErrorResponse) error {

	if utils.IsEmpty(memberID) {
		logger.Log().WithContext(ctx).Error("MemberID is epmty")
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.MemberIdKey, consts.RequiredKey)
		if err != nil {
			logger.Log().WithContext(ctx).Error("IsMemberExists failed", err)
		}

		errMap[consts.MemberIdKey] = models.ErrorResponse{
			Code:    code,
			Message: []string{consts.RequiredKey},
		}
		return err
	}

	_, err := uuid.Parse(memberID)
	if err != nil {
		logger.Log().WithContext(ctx).Error("IsMemberExists failed", err)
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.MemberIdKey, consts.InvalidKey)
		if err != nil {
			logger.Log().WithContext(ctx).Error("IsMemberExists failed", err)
		}

		errMap[consts.MemberIdKey] = models.ErrorResponse{
			Code:    code,
			Message: []string{consts.InvalidKey},
		}
		return err
	}

	exists, err := utilities.IsMemberExists(ctx, memberID, consts.MemberApiURL)
	if err != nil {
		logger.Log().WithContext(ctx).Error("IsMemberExists failed", err)
		return err
	}
	if !exists {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.MemberIdKey, consts.NotFoundKey)
		if err != nil {
			logger.Log().WithContext(ctx).Errorf(consts.CreateCommentsErrMsg, err)
		}
		errMap[consts.MemberIdKey] = models.ErrorResponse{
			Code:    code,
			Message: []string{consts.NotFoundKey},
		}
	}

	return nil
}

func (product *ProductUseCases) CreateComments(ctx context.Context, productID string, MemberID string, comments entities.ReviewComments, endpoint string, method string, errMap map[string]models.ErrorResponse) (map[string]models.ErrorResponse, error) {
	reviewComments := strings.TrimSpace(comments.Comments)
	if utils.IsEmpty(reviewComments) {
		code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.CommentsKey, consts.RequiredKey)
		if err != nil {
			logger.Log().WithContext(ctx).Errorf(consts.CreateCommentsErrMsg, err)
		}
		errMap[consts.CommentsKey] = models.ErrorResponse{
			Code:    code,
			Message: []string{consts.RequiredKey},
		}
	} else {
		if len(reviewComments) > consts.MaxCommentLength {
			code, err := utils.GetErrorCode(consts.ErrorCodeMap, endpoint, method, consts.CommentsKey, consts.LimitExceeds)
			if err != nil {
				logger.Log().WithContext(ctx).Errorf(consts.CreateCommentsErrMsg, err)
			}
			errMap[consts.CommentsKey] = models.ErrorResponse{
				Code:    code,
				Message: []string{consts.LimitExceeds},
			}
		}

	}
	if len(errMap) != 0 {
		return errMap, nil
	}
	return nil, product.repo.CreateComments(ctx, productID, MemberID, comments)

}
func (product *ProductUseCases) ExtractMemberDetails(ctx context.Context, memberID string) (string, error) {
	log := logger.Log().WithContext(ctx)

	// Prepare API URL with parameters
	apiURL := fmt.Sprintf("%s/%s/%s", consts.MemberApiURL, "members", memberID)
	// Make the API request
	headers := make(map[string]interface{})
	headers["Content-Type"] = "application/json"

	response, err := utils.APIRequest("GET", apiURL, headers, nil)
	if err != nil {

		log.Printf("failed to make API request: %v", err)
		return "", fmt.Errorf("failed to connect member service")
	}
	defer response.Body.Close()

	// Read response body
	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Printf("failed to read response body: %v", err)
		return "", err
	}

	// Unmarshal JSON response
	var jsonResponse entities.MemberResponse
	err = json.Unmarshal(body, &jsonResponse)

	if err != nil {
		log.Printf("failed to unmarshal JSON response: %v", err)
		return "", err
	}

	// Extract Member details
	firstName := jsonResponse.MemberData.MemberDetails.FirstName
	lastName := jsonResponse.MemberData.MemberDetails.LastName

	name := fmt.Sprintf("%s %s", firstName, lastName)

	if utils.IsEmpty(name) {
		email := jsonResponse.MemberData.MemberDetails.Email
		return email, nil
	}

	return name, nil
}
