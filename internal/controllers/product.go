package controllers

import (
	"fmt"
	"product/internal/consts"
	"product/internal/entities"
	"product/internal/usecases"
	"product/utilities"
	"strings"
	"time"

	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/tuneverse/toolkit/core/activitylog"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/models/api"
	"gitlab.com/tuneverse/toolkit/utils"
)

// ProductController is a struct that manages the HTTP routing and interaction
// with the use cases for handling Products.
type ProductController struct {
	router      *gin.RouterGroup
	useCases    usecases.ProductUseCaseImply
	Cfg         *entities.EnvConfig
	activitylog *activitylog.ActivityLogOptions
}

// NewMemberController creates a new instance of MemberController.
func NewProductController(router *gin.RouterGroup, memberUseCase usecases.ProductUseCaseImply, cfg *entities.EnvConfig, activitylog *activitylog.ActivityLogOptions) *ProductController {
	return &ProductController{
		router:      router,
		useCases:    memberUseCase,
		Cfg:         cfg,
		activitylog: activitylog,
	}
}

// InitRoutes initializes the routes for the ProductController.
func (product *ProductController) InitRoutes() {

	product.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, product, "Health")
	})

	product.router.POST("/:version/products/:product_id/review/comments", func(ctx *gin.Context) {
		version.RenderHandler(ctx, product, "CreateComments")
	})

}

// Health handles the health endpoint.
func (product *ProductController) Health(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

func (product *ProductController) CreateComments(ctx *gin.Context) {
	var (
		log      = logger.Log().WithContext(ctx)
		comments entities.ReviewComments
	)
	errMap := utilities.NewErrorMap()
	serviceCode := make(map[string]string)
	helpLink := consts.ErrorHelpLink
	loggerVar := logger.Log().WithContext(ctx.Request.Context())
	method := strings.ToLower(ctx.Request.Method)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[[]models.DataItem](ctx, consts.ContextEndPoints)
	contextError, errVal := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if !errVal {
		loggerVar.Error(consts.CreateCommentsErrMsg, consts.ContextErrMsg)
		return
	}
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)
	if !isEndpointExists {
		val, _, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		log.Errorf(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.InvalidEndpointErrMsg, val))
		result := api.Response{Status: consts.FailureKey, Message: errDet.Message, Code: int(errorCode), Data: nil, Errors: map[string]string{}}
		ctx.JSON(http.StatusBadRequest, result)
		return
	}
	memberID := ctx.Request.Header.Get(consts.MemberIdKey)
	err := product.useCases.IsMemberExists(ctx, memberID, endpoint, method, errMap)
	if len(errMap) != 0 {
		for key, value := range errMap {
			serviceCode[key] = value.Code
		}
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LocalizationModuleErrMsg, val))
		}
		result := api.Response{Status: consts.FailureKey, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}
	if err != nil {
		loggerVar.Errorf(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LogErrMsg, err.Error()))
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LocalizationModuleErrMsg, val))
		}
		loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.InvalidEndpointErrMsg, val))
		result := utilities.ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusInternalServerError, result)
		return

	}
	ProductID := ctx.Param(consts.ProductIDKey)
	productExists, err := product.useCases.IsProductExists(ctx, ProductID, endpoint, method, errMap)
	if !productExists {
		for key, value := range errMap {
			serviceCode[key] = value.Code
		}
		fields := utils.FieldMapping(errMap)
		val, hasError, _, _ := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LocalizationModuleErrMsg, val))
		}
		result := api.Response{Status: consts.FailureKey, Message: consts.PathParameterErrMsg, Code: http.StatusNotFound, Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusNotFound,
			result,
		)
		return
	}
	if err != nil {
		loggerVar.Errorf(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LogErrMsg, err.Error()))
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LocalizationModuleErrMsg, val))
		}
		loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.InvalidEndpointErrMsg, val))
		result := utilities.ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusInternalServerError, result)
		return
	}

	err = ctx.BindJSON(&comments)

	if err != nil {
		log.Errorf(consts.CreateCommentsErrMsg, err.Error())
		result := utilities.ErrorResponseGenerator(consts.BindingErrorErrMsg, http.StatusBadRequest, consts.ParsingErrMsg)
		ctx.JSON(http.StatusBadRequest,
			result,
		)

	}
	errMap, err = product.useCases.CreateComments(ctx, ProductID, memberID, comments, endpoint, method, errMap)
	// validation error check
	if len(errMap) != 0 {
		for key, value := range errMap {
			serviceCode[key] = value.Code
		}
		fields := utils.FieldMapping(errMap)
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method, serviceCode, helpLink)
		if hasError {
			loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LocalizationModuleErrMsg, val))
		}
		result := api.Response{Status: consts.FailureKey, Message: errDet.Message, Code: int(errorCode), Data: map[string]string{}, Errors: val}
		ctx.JSON(http.StatusBadRequest,
			result,
		)
		return
	}
	if err != nil {
		loggerVar.Errorf(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LogErrMsg, err.Error()))
		val, hasError, errorCode, errDet := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "", nil, helpLink)
		if !hasError {
			loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.LocalizationModuleErrMsg, val))
		}
		loggerVar.Error(consts.CreateCommentsErrMsg, fmt.Sprintf(consts.InvalidEndpointErrMsg, val))
		result := utilities.ErrorResponseGenerator(errDet.Message, int(errorCode), "")

		ctx.JSON(http.StatusInternalServerError, result)
		return

	}

	result := utilities.SuccessResponseGenerator(consts.CreateCommentsSuccessMsg, http.StatusCreated, "")
	// ***************Activity code starts here********************
	name, err := product.useCases.ExtractMemberDetails(ctx, memberID)
	if err != nil {
		log.Errorf(consts.CreateCommentsErrMsg, err.Error())
	}

	data := map[string]interface{}{
		consts.ProductBaseUrlKey: consts.ProductBaseUrl,
		consts.ProductIDKey:      ProductID,
		consts.MemberNameKey:     name,
		consts.DateTimeKey:       time.Now(),
	}

	_, ok := data[consts.ProductIDKey].(string)
	if ok {
		activityDetails := utilities.NewActivityLog(memberID, consts.ReviewCommentActvityLogKey, data)

		resp, err := product.activitylog.Log(activityDetails)
		if err != nil {
			logger.Log().WithContext(ctx.Request.Context()).Error(consts.ActivityLogErrMsg, err.Error(), resp)

		}
	}

	// ***************Activity code ends here********************
	ctx.JSON(http.StatusCreated, result)

}
