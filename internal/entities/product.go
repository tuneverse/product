package entities

// Struct used for storing details
type ReviewComments struct {
	Comments string `json:"comments"`
}
type MemberResponse struct {
	MemberData MemberData `json:"data"`
}

type MemberData struct {
	MemberDetails struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Email     string `json:"email"`
	} `json:"member_details"`
}
