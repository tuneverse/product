package entities

// EnvConfig represents the configuration structure for the application.
type EnvConfig struct {
	Debug            bool         `default:"true" split_words:"true"`  // Flag indicating debug mode (default: true)
	Port             int          `default:"8080" split_words:"true"`  // Port for server to listen on (default: 8080)
	Db               PsqlDatabase `split_words:"true"`                 // Database configuration
	AcceptedVersions []string     `required:"true" split_words:"true"` // List of accepted API versions (required)
	MigrationPath    string       `split_words:"true"`
	AWS              AWS
}

type PsqlDatabase struct {
	User      string `split_words:"true"`
	Password  string `split_words:"true"`
	Port      int    `split_words:"true"`
	Host      string `split_words:"true"`
	DATABASE  string `split_words:"true"`
	Schema    string `split_words:"true"`
	MaxActive int    `split_words:"true"`
	MaxIdle   int    `split_words:"true"`
}

// AWS represents AWS configuration settings, including access key, access secret, and region.
type AWS struct {
	AccessKey    string `split_words:"true"`
	AccessSecret string `split_words:"true"`
	Region       string
	BucketName   string `split_words:"true"`
}
