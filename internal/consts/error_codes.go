package consts

// Add values to the map
var ErrorCodeMap = map[string]interface{}{
	"review_comments": map[string]interface{}{
		"post": map[string]interface{}{
			"member_id": map[string]interface{}{
				"invalid":   "A9867",
				"required":  "A4567",
				"not_found": "A2345",
			},
			"product_id": map[string]interface{}{
				"invalid":   "A9867",
				"not_found": "A2345",
			},
			"comments": map[string]interface{}{
				"required":      "A4589",
				"limit_exceeds": "A7678",
			},
		},
	},
}
