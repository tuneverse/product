package consts

import "os"

// AppName stores the Application name
const (
	AppName      = "product"
	DatabaseType = "postgres"
)

const (
	AcceptedVersions = "v1"
)

const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
	ContextEndPoints              = "context-endpoints"
)

// Context setting values
const (
	ContextErrorResponses        = "context-error-response"
	ContextLocallizationLanguage = "lan"
)

// headers
const (
	HeaderLocallizationLanguage = "Accept-Language"
)

// cache name
const CacheErrorData = "CACHE_ERROR_DATA"

const ExpiryTime = 180

// KeyNames
const (
	ValidationErr      = "validation_error"
	ForbiddenErr       = "forbidden"
	UnauthorisedErr    = "unauthorized"
	NotFound           = "not found"
	InternalServerErr  = "internal_server_error"
	Errors             = "errors"
	AllError           = "AllError"
	ErrorCode          = "errorCode"
	MemberIdErr        = "member_id"
	PostBillingAddress = "post_billing_address"
)

const (
	Success = "Successfully added the details"
)

const (
	ProductIDKey               = "product_id"
	MemberIdKey                = "member_id"
	RequiredKey                = "required"
	InvalidKey                 = "invalid"
	LimitExceeds               = "limit_exceeds"
	NotFoundKey                = "not_found"
	CommentsKey                = "comments"
	SuccessKey                 = "success"
	FailureKey                 = "failure"
	DateTimeKey                = "date_time"
	MemberNameKey              = "member_name"
	ProductBaseUrlKey          = "base_url_product"
	ReviewCommentActvityLogKey = "product_review_comments_created"
)

const (
	CacheErrorKey     = "ERROR_CACHE_KEY_LABEL"
	CacheEndpointsKey = "endpoints"
)

const (
	CreateCommentsErrMsg     = "Create Comments failed err=%s"
	ContextErrMsg            = "failed to fetch error values from context"
	InvalidEndpointErrMsg    = "invalid endpoint %s"
	LocalizationModuleErrMsg = "error occured while retrieving errors from localization module %s"
	LogErrMsg                = "failed to retrieve logs, err=%s"
	PathParameterErrMsg      = "path parameter error"
	EndpointErr              = "Error occured while loading endpoints from service"
	ContextErr               = "Error occured while loading error from service"
	BindingErrorErrMsg       = "binding error"
	ParsingErrMsg            = "error occured while parsing"
	ActivityLogErrMsg        = "Activity log failed: err=%s"
)

const (
	CreateCommentsSuccessMsg = "comments created successfully"
)

const (
	MaxCommentLength = 2000
)

var (
	ErrorHelpLink                = os.Getenv("PRODUCT_ERROR_HELP_LINK")
	MemberApiURL                 = os.Getenv("PRODUCT_MEMBER_SERVICE_URL")
	ActivityLogServiceURL        = os.Getenv("PRODUCT_ACTIVITY_LOG_SERVICE_URL")
	ErrorLocalizationURL         = os.Getenv("PRODUCT_LOCALISATION_SERVICE_URL")
	LoggerServiceURL             = os.Getenv("PRODUCT_LOGGER_SERVICE_URL")
	LoggerSecret                 = os.Getenv("PRODUCT_LOGGER_SECRET")
	ErrorLocalizationEndpointURL = os.Getenv("PRODUCT_ENDPOINT_URL")
	ProductBaseUrl               = os.Getenv("PRODUCT_BASE_URL")
)
