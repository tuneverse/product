package middlewares

import (
	"product/internal/consts"
	"product/internal/entities"

	"github.com/gin-gonic/gin"
)

// Middlewares structure for storing middleware values
type Middlewares struct {
	Cfg *entities.EnvConfig
}

// NewMiddlewares creates a new middleware object
func NewMiddlewares(cfg *entities.EnvConfig) *Middlewares {
	return &Middlewares{
		Cfg: cfg,
	}
}

// LocalizationLanguage
func (m Middlewares) LocalizationLanguage() gin.HandlerFunc {
	return func(c *gin.Context) {
		// check it is exists in the header
		lan := c.Request.Header.Get(consts.HeaderLocallizationLanguage)
		if lan == "" {
			lan = ""
		}

		// setting the language
		c.Set(consts.ContextLocallizationLanguage, lan)

		c.Next()
	}
}
