package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	conf "product/config"
	"product/internal/cloud/awsutils"
	"product/internal/consts"
	"product/internal/controllers"
	"product/internal/entities"
	"product/internal/repo"
	"product/internal/repo/driver"
	"product/internal/usecases"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/patrickmn/go-cache"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/tuneverse/toolkit/core/activitylog"
	"gitlab.com/tuneverse/toolkit/core/awsmanager"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
	"gitlab.com/tuneverse/toolkit/utils"
)

// Run method to run the service in gin framework
// env configuration
// logrus, zap
// use case intia
// repo initalization
// controller init
func Run() {
	// init the env config
	cfg, err := conf.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	var log *logger.Logger

	// Create a file logger configuration with specified settings.
	file := &logger.FileMode{
		LogfileName:  "product.log",    // Log file name.
		LogPath:      "logs",           // Log file path. Add this folder to .gitignore as logs/
		LogMaxAge:    7,                // Maximum log file age in days.
		LogMaxSize:   1024 * 1024 * 10, // Maximum log file size (10 MB).
		LogMaxBackup: 5,                // Maximum number of log file backups to keep.
	}

	// Configure client options for the logger.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
		JSONFormater:        false,          // log format, if sets to false, default text formatter is used.
	}

	if cfg.Debug {
		// Debug Mode: Logs will print to both the file and the console.
		// Initialize the logger with the specified configurations for file and console logging.
		log = logger.InitLogger(clientOpt, file)
	} else {
		// Create a database logger configuration with the specified URL and secret.
		db := &logger.CloudMode{
			URL:    consts.LoggerServiceURL,
			Secret: consts.LoggerSecret,
		}
		// Initialize the logger with the specified configurations for database, file, and console logging.
		log = logger.InitLogger(clientOpt, db, file)
	}

	//initialises aws
	awsConfig, err := awsInit(cfg)
	if err != nil {
		log.Fatalf("Failed to initialize aws, err=%s", err.Error())
		return
	}

	activitylog, err := activitylog.Init(consts.ActivityLogServiceURL)
	if err != nil {
		log.Fatalf("unable to connect the activity log service : %v", err)
		return
	}

	// psqlDB, err := driver.(cfg.Db)
	psqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database : %v", err)
		return
	}

	// // here initalizing the router
	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	api := router.Group("/api")
	// middleware initialization
	api.Use(middleware.LogMiddleware(map[string]interface{}{}))
	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))
	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          consts.CacheErrorKey,
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", consts.ErrorLocalizationURL),
		},
	))
	api.Use(middleware.EndpointExtraction(
		middleware.EndPointOptions{
			Cache:            cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:  time.Duration(time.Hour * 24),
			CacheKeyLabel:    consts.CacheEndpointsKey,
			ContextEndPoints: consts.ContextEndPoints,
			EndPointsURL:     fmt.Sprintf("%s/localization/endpointname", consts.ErrorLocalizationEndpointURL),
		},
	))
	{
		{
			//cloud initialization
			cloud := awsutils.NewCloudService(awsConfig)
			// repo initialization
			repo := repo.NewProductRepo(psqlDB)

			// initilizing usecases
			useCases := usecases.NewProductUseCases(repo, cloud)

			// initalizin controllers
			controller := controllers.NewProductController(api, useCases, cfg, activitylog)

			// init the routes
			controller.InitRoutes()
		}

	}
	// runn the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Log().Fatalf("listen: %s\n", err)
		}
	}()
	fmt.Println("Server listening in...", cfg.Port)
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown: %s", err.Error())
	}
	// catching ctx.Done(). timeout of 5 seconds.
	select {
	case <-ctx.Done():
		log.Println("timeout of 5 seconds.")
	}
	log.Println("Server exiting")
}

func awsInit(cfg *entities.EnvConfig) (*awsmanager.AwsConfig, error) {
	var optFns []func(*config.LoadOptions) error
	if !utils.IsEmpty(cfg.AWS.AccessKey) && !utils.IsEmpty(cfg.AWS.AccessSecret) {
		optFns = append(optFns, awsmanager.WithCredentialsProvider(cfg.AWS.AccessKey, cfg.AWS.AccessSecret))
	}
	if !utils.IsEmpty(cfg.AWS.Region) {
		optFns = append(optFns, awsmanager.WithRegion(cfg.AWS.Region))
	}
	awsConf, err := awsmanager.CreateAwsSession(optFns...)
	if err != nil {
		return nil, err
	}
	return awsConf, nil
}
