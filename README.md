# Product Service

This module is responsible for the comprehensive management of products, which includes creating new products, updating existing ones, validating their details to ensure accuracy and compliance, and managing the flow of their status throughout their lifecycle.

## Environment variables and description

    PRODUCT_DEBUG: Enables or disables debug mode for the application. When set to true, debug information is displayed.
    PRODUCT_DB_PORT: The port number on which the product's database server is running. Default is set to 5433.
    PRODUCT_DB_USER: The username used to connect to the product's database. Here, it is set to postgres.
    PRODUCT_DB_PASSWORD: The password for the database user. In this case, it is tuneverse.
    PRODUCT_DB_DATABASE: The name of the database used by the product. The value here is tuneverse_local.
    PRODUCT_ACCEPTED_VERSIONS: Specifies the accepted versions of the product's API. In this case, it is set to "v1_0".
    PRODUCT_DB_SCHEMA: The schema within the database that is used by the product. Here, it is set to public.
    PRODUCT_DB_HOST: The host address of the product's database server. It is set to 10.1.0.229.
    PRODUCT_LOCALISATION_SERVICE_URL: The URL of the localisation service used by the product. It is set to http://10.1.0.206:8025/api/v1.
    PRODUCT_CACHE_EXPIRATION: The duration for which cached data is valid. The value is currently empty, indicating no specific cache expiration set.
    PRODUCT_ENDPOINT_URL: The base URL for the product's API endpoints. It is set to http://10.1.0.206:8025/api/v1.
    PRODUCT_LOGGER_SERVICE_URL: The URL of the logging service used by the product. It is set to http://10.1.0.226:8040/api/v1.0.
    PRODUCT_LOGGER_SECRET: The secret key used for authentication with the logging service. The value is "logger secret".
    PRODUCT_AWS_ACCESS_KEY: The AWS access key used for accessing AWS services. The value is AKIAWJP3Y6XDOJQP6QHT.
    PRODUCT_AWS_ACCESS_SECRET: The AWS access secret corresponding to the access key. The value is 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.
    PRODUCT_AWS_REGION: The AWS region where the product's resources are hosted. It is set to us-east-1.
    PRODUCT_AWS_BUCKET_NAME: The name of the AWS S3 bucket used by the product. The value is tuneversev2.
    PRODUCT_ERROR_HELP_LINK: A link to a document or spreadsheet containing help information for product errors. The link is https://docs.google.com/spreadsheets/d/1dgBRdaj-xVt5DcrBNIEjqjzH5paAmKyKJ4DeihXvcDo/edit?usp=sharing.
    PRODUCT_MEMBER_SERVICE_URL: The URL of the member service used by the product. It is set to http://10.1.0.137:8039/api/v1.0.
    PRODUCT_TRACK_SERVICE_URL: The URL of the track service used by the product. It is set to http://10.1.0.206:8028/api/v1.0.
    PRODUCT_LABEL_SERVICE_URL: The URL of the label service used by the product. It is set to http://10.1.0.111:8021/api/v1.0.
    PRODUCT_STORE_SERVICE_URL: The URL of the store service used by the product. It is set to http://10.1.0.108:8022/api/v1.0.
    PRODUCT_ACTIVITY_LOG_SERVICE_URL: The URL of the activity log service used by the product. It is set to http://10.1.0.206:8026/api/v1.0.