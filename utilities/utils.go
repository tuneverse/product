package utilities

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"product/internal/consts"

	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/models/api"
	"gitlab.com/tuneverse/toolkit/utils"
)

// For success response
func SuccessResponseGenerator(message string, code int, data any) api.Response {
	var result api.Response
	if data == "" {
		data = map[string]interface{}{}
	}
	result = api.Response{Status: consts.SuccessKey, Message: message, Code: code, Data: data, Errors: map[string]string{}}
	return result
}

// For error response
func ErrorResponseGenerator(message string, code int, errors any) api.Response {
	var result api.Response
	if errors == "" {
		errors = map[string]interface{}{}
	}
	result = api.Response{Status: consts.FailureKey, Message: message, Code: code, Data: map[string]string{}, Errors: errors}
	return result
}
func IsMemberExists(ctx context.Context, memberId string, apiUrl string) (bool, error) {

	apiURL := fmt.Sprintf("%s/%s/%s", apiUrl, "members", memberId)
	headers := make(map[string]interface{})
	headers["Content-Type"] = "application/json"

	response, err := utils.APIRequest("HEAD", apiURL, headers, nil)
	if err != nil {
		log.Printf("failed to make API request: %v", err)
		return false, fmt.Errorf("failed to connect member service")
	}
	defer response.Body.Close()
	return response.StatusCode == http.StatusOK, nil
}
func NewActivityLog(memberID, action string, data map[string]interface{}) models.ActivityLog {
	return models.ActivityLog{
		MemberID: memberID,
		Action:   action,
		Data:     data,
	}
}
